package com.radmag.sevengin;

import android.graphics.*;
import com.radmag.sevengin.components.*;
import java.util.*;
import com.radmag.sevengin.utility.*;

public class Screen
{
	private ArrayList<Sprite> components = new ArrayList<Sprite>();
	protected GinCamera camera = new GinCamera();
	private Bitmap background;
	
	Paint paint = new Paint();
	
	public Screen(FlatPoint resolution)
	{
		camera = new GinCamera(resolution);
	}
	
	public Screen(GinCamera camera)
	{
		this.camera = camera;
	}
	
	public Screen()
	{
		
	}
	
	public void update()
	{
		Collections.sort(components);
		for(Sprite iObj : components)
		{
			iObj.update();
		}
	}
	
	public void render(Canvas canvas, float tc)
	{
		
		paint.setColor(Color.BLACK);
		canvas.drawRect(0,0,1280,720,paint);
		FlatPoint co = camera.getOffset();
		canvas.drawBitmap(background, 0.0f + co.x,0.0f + co.y, paint);
		for (Sprite iSprite : components)
		{
			iSprite.render(canvas, camera, tc);
		}
	}
	
	public ArrayList<Sprite> getComponents()
	{
		return components;
	}
	
	public void AddComponent(Sprite component)
	{
		synchronized(components)
		{
			components.add(component);
		}
	}
	
	public GinCamera getCamera()
	{
		return camera;
	}
	
	public void setCamera(GinCamera camera)
	{
		this.camera = camera;
	}
	
	public void setBackground(Bitmap background)
	{
		this.background = background;
		
	}
	
	public Bitmap getBackground()
	{
		return background;
	}
	
}
