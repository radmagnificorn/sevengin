package com.radmag.sevengin.collision;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.utility.*;

/**
* Holds information on what parts of the map can be walked on
*/
public class BoundingMap
{
	int[] map;
	int width;
	Bitmap bmap;
	
	/**
	* @param mapFileId R based id of bounding image in resources
	*/
	public BoundingMap(int mapFileId)
	{
		
		Bitmap tempMap = ResourceLoader.get().loadBitmap(mapFileId);
		width = tempMap.getWidth();
		map = new int[tempMap.getWidth() * tempMap.getHeight()];
		tempMap.getPixels(map, 0, width, 0, 0, width, tempMap.getHeight());
		bmap = tempMap;
	}
	
	/**
	* Check if a point is inside or outside the walkable area
	* @param cx point x coordinate
	* @param cy point y coordinate
	* @return true=point out of bounds - false=no boundry collision
	*/
	public boolean checkCollision(int cx, int cy)
	{
		int pixel = map[cy * width + cx];
		//int pixel = bmap.getPixel((int)cPoint.x, (int)cPoint.y);
		return pixel != -1;
	}
	
	/**
	* @return color at specified point
	*/
	public int checkColor(int cx, int cy)
	{
		int pixel = map[cy * width + cx];
		//int pixel = bmap.getPixel((int)cPoint.x, (int)cPoint.y);
		return pixel;
	}
}
