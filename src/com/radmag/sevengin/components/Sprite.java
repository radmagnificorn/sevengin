package com.radmag.sevengin.components;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.utility.*;
import java.util.*;

public class Sprite implements Comparable<Sprite>
{
	private GinPoint position;
	private GinPoint velocity;
	private GinPoint size;
	private GinPoint lastRenderPoint;
	private Bitmap image;

	protected boolean interpolate = true;
	protected Paint paint = new Paint();

	private ArrayList<Component> components = new ArrayList<Component>();

	public Sprite(GinPoint position, GinPoint size, GinPoint velocity)
	{
		this.position = position;
		this.size = size;
		this.velocity = velocity;
		lastRenderPoint = position.copy();
	}
	public Sprite(GinPoint position, GinPoint velocity)
	{
		this.position = position;
		this.velocity = velocity;
	}
	public Sprite(GinPoint position)
	{
		this(position, new GinPoint(0, 0, 0));
	}
	public Sprite()
	{
		this(new GinPoint(0, 0, 0), new GinPoint(0, 0, 0));
	}

	public void setImage(Bitmap image)
	{
		this.image = image;
	}

	public Bitmap getImage()
	{
		return image;
	}

	public void setPosition(GinPoint position)
	{
		this.position = position;
	}
	public GinPoint getPosition()
	{
		return position;
	}

	public GinPoint getVelocity()
	{
		return velocity;
	}

	public void setVelocity(GinPoint velocity)
	{
		this.velocity = velocity;
	}

	public GinPoint getSize()
	{
		return size;
	}

	public void setSize(GinPoint size)
	{
		this.size = size;
	}

	public float getHeight()
	{
		return size.y;
	}

	public float getWidth()
	{
		return size.x;
	}
	public void setHeight(float height)
	{
		size.y = height;
	}
	public void setWidth(float width)
	{
		size.x = width;
	}
	public float getDepth()
	{
		return size.z;
	}

	public void update()
	{
		for (Component com : components)
		{
			com.update();
		}
		position = position.plus(velocity);
	}

	public void render(Canvas canvas, GinCamera camera, float interp)
	{
		FlatPoint co = camera.getOffset();
		canvas.drawBitmap(image, position.x + co.x, position.y + co.y, paint);
	}	

	public void addComponent(Component com)
	{
		com.initialize(this);
		components.add(com);
	}

	public GinPoint getInterpPosition(float tc)
	{
		GinPoint renderPoint;
		if (interpolate)
		{
			renderPoint = new GinPoint(interp(position.x, velocity.x, tc),
									   interp(position.y, velocity.y, tc),
									   interp(position.z, velocity.z, tc));
			// If the difference between the last and proposed render points
			// is greater than the velocities, then the position has updated since
			// we took the interpolation percentage
			if (Math.abs(renderPoint.x - lastRenderPoint.x) > Math.abs(this.getVelocity().x))
			{
				// don't apply like, 98% interpolation if the update loop just reset
				renderPoint.x = this.getPosition().x;
			}
			if (Math.abs(renderPoint.y - lastRenderPoint.y) > Math.abs(this.getVelocity().y))
			{
				// don't apply like, 98% interpolation if the update loop just reset
				renderPoint.y = this.getPosition().y;
			}
			if (Math.abs(renderPoint.z - lastRenderPoint.z) > Math.abs(this.getVelocity().z))
			{
				// don't apply like, 98% interpolation if the update loop just reset
				renderPoint.z = this.getPosition().z;
			}
			lastRenderPoint = renderPoint.copy();
		}
		else
		{
			renderPoint = this.position;
		}

		return renderPoint;

	}

	public static float interp(float position, float velocity, float tc)
	{
		return position + (velocity * tc);
	}


	public int compareTo(Sprite otherSprite)
	{

		if (this.position.z < otherSprite.position.z)
		{
			return 1;
		}
		else if (this.position.z > otherSprite.position.z)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
