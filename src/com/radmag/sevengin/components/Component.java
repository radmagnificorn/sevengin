package com.radmag.sevengin.components;

/**
* Component based architecture to add properties to game objects/sprites
*/
public interface Component
{
	/**
	* called automatically when component is added to sprite
	* @param sprite The component's parent sprite
	*/
	public void initialize(Sprite sprite);
	/**
	 * called within parent sprite's update method
	 */
	public void update();
}
