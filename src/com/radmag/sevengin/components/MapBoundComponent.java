package com.radmag.sevengin.components;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.collision.*;
import com.radmag.sevengin.utility.*;

public class MapBoundComponent implements Component
{
	private BoundingMap map;
	private float boxDepth;
	private float boxWidth;
	private Sprite sprite;
	private GinCamera camera;
	private CheckPoint[] checkPoints = new CheckPoint[8];
	
	public int leftColor = 0;
	
	public MapBoundComponent(BoundingMap map, GinCamera camera)
	{
		this.map = map;
		this.camera = camera;
	}
	public MapBoundComponent(BoundingMap map,float boxWidth, float boxDepth, GinCamera camera)
	{
		this.map = map;
		this.boxDepth = boxDepth;
		this.boxWidth = boxWidth;
		this.camera = camera;
	}
	
	public void initialize(Sprite sprite)
	{
		this.sprite = sprite;
		//Calcluate bounding box size
		if (boxDepth == 0.0f)
		{
			if (sprite.getDepth() != 0)
			{
				boxDepth = sprite.getDepth();
			} else {
				boxDepth = sprite.getWidth();
			}
		}
		if (boxWidth == 0.0f)
		{
			boxWidth = sprite.getWidth();
		}
		float height = sprite.getHeight()*0.9f;
		float edge = (sprite.getWidth() - boxWidth) * 0.5f;
		
		//Generate box points
		checkPoints[TOP_LEFT] = new CheckPoint(new GinPoint(edge, height, boxDepth*0.5f));
		checkPoints[TOP] = new CheckPoint(new GinPoint(edge+boxWidth*0.5f, height, boxDepth*0.5f));
		checkPoints[TOP_RIGHT] = new CheckPoint(new GinPoint(edge+boxWidth, height, boxDepth*0.5f));
		checkPoints[RIGHT] = new CheckPoint(new GinPoint(edge+boxWidth, height, 0.0f));
		checkPoints[BOTTOM_RIGHT] = new CheckPoint(new GinPoint(edge+boxWidth, height, boxDepth*-0.5f));
		checkPoints[BOTTOM] = new CheckPoint(new GinPoint(edge+boxWidth * 0.5f, height, boxDepth*-0.5f));
		checkPoints[BOTTOM_LEFT] = new CheckPoint(new GinPoint(edge, height, boxDepth*-0.5f));
		checkPoints[LEFT] = new CheckPoint(new GinPoint(edge, height, 0.0f));
		
	}
	public void render(Canvas canvas)
	{
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		for(int i=0; i<checkPoints.length; i++)
		{
			CheckPoint iPoint = checkPoints[i];
			FlatPoint point = camera.translatePoint(sprite.getPosition().plus(iPoint.point));
			canvas.drawRect(point.x, point.y, point.x + 5.0f, point.y + 5.0f, paint); 
		}
	}
	public void update()
	{
		//check points for collision
		for(int i=0; i<checkPoints.length; i++)
		{
			CheckPoint iPoint = checkPoints[i];
			FlatPoint point = camera.translatePoint(iPoint.point.plus(sprite.getPosition()), false);
			iPoint.triggered = map.checkCollision((int)point.x, (int)point.y);
			if (i == LEFT)
			{
				leftColor = map.checkColor((int)point.x, (int)point.y);
			}
		}
		float xVel = sprite.getVelocity().x;

		if (checkPoints[TOP_LEFT].triggered)
		{
			if (sprite.getVelocity().z > 0 && !checkPoints[RIGHT].triggered
			&& !checkPoints[TOP].triggered)
			{
				sprite.getPosition().x += sprite.getVelocity().z;
				//sprite.getVelocity().z = 0;
			}
			/* --- Left points are currently too close for this to be useful*/
			if (sprite.getVelocity().x < 0 && !checkPoints[BOTTOM_LEFT].triggered
			&& !checkPoints[LEFT].triggered)
			{
				sprite.getPosition().z += Math.abs(sprite.getVelocity().x);
			}
			
		}
		
		if (checkPoints[TOP_RIGHT].triggered)
		{
			if (sprite.getVelocity().z > 0 && !checkPoints[LEFT].triggered
			&& !checkPoints[TOP].triggered)
			{
				sprite.getPosition().x -= sprite.getVelocity().z;
			}
		}
		
		if (checkPoints[BOTTOM_LEFT].triggered)
		{
			if (sprite.getVelocity().z < 0 && !checkPoints[RIGHT].triggered
				&& !checkPoints[BOTTOM].triggered)
			{
				sprite.getPosition().x += Math.abs(sprite.getVelocity().z);
			}
		}
		
		if (checkPoints[BOTTOM_RIGHT].triggered)
		{
			if (sprite.getVelocity().z < 0 && !checkPoints[LEFT].triggered
				&& !checkPoints[BOTTOM].triggered)
			{
				sprite.getPosition().x -= Math.abs(sprite.getVelocity().z);
			}
		}
		
		if (checkPoints[LEFT].triggered && xVel < 0)
		{
			sprite.getVelocity().x = 0;
		}
		if (checkPoints[RIGHT].triggered && xVel > 0)
		{
			sprite.getVelocity().x = 0;
		}
		float zVel = sprite.getVelocity().z;
		
		if (checkPoints[TOP].triggered && zVel > 0)
		{
			sprite.getVelocity().z = 0;
		}
		if (checkPoints[BOTTOM].triggered && zVel < 0)
		{
			sprite.getVelocity().z = 0;
		}
		
		
	}
	
	public void setBoundingMap(BoundingMap map)
	{
		this.map = map;
	}
	
	private class CheckPoint
	{
		public GinPoint point;
		public boolean triggered = false;
		
		public CheckPoint(GinPoint point)
		{
			this.point = point;
		}
	}

	private static final int 
		TOP_LEFT = 0,
		TOP = 1,
		TOP_RIGHT = 2,
		RIGHT = 3,
		BOTTOM_RIGHT = 4,
		BOTTOM = 5,
		BOTTOM_LEFT = 6,
		LEFT = 7;
	
}
