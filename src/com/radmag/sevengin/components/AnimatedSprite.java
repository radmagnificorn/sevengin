package com.radmag.sevengin.components;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.utility.*;
import java.util.*;

public class AnimatedSprite extends Sprite
{
	protected Bitmap image;
	protected Animation currentAnimation;
	protected Point frameSize = new Point(50,50);
	protected Paint paint = new Paint();
	protected HashMap<String, Animation> animations = new HashMap<String, Animation>();
	
	public AnimatedSprite(GinPoint position, GinPoint size, GinPoint velocity)
	{
		super(position, size, velocity);
	}
	public void setImage(Bitmap image)
	{
		this.image = image;
	}
	
	public void setAnimation(String animationName)
	{
		currentAnimation = animations.get(animationName);
	}
	
	public void playAnimation(String animationName)
	{
		currentAnimation = animations.get(animationName);
		currentAnimation.play();
	}
	
	@Override
	public void update()
	{
		if (currentAnimation != null)
		{	
			currentAnimation.update();
		}
		super.update();
	}
	
	public Rect getFrame()
	{
		Point frame = currentAnimation.getFrame();
		return new Rect(frame.x, frame.y, 
					frame.x + frameSize.x,
					frame.y + frameSize.y);
	}
	
	@Override
	public void render(Canvas canvas, GinCamera camera, float tc)
	{
		GinPoint rendPos;
		//Get the interpolated position
		if (tc != 0)
		{
			rendPos = this.getInterpPosition(tc);
		}
		else
		{
			rendPos = this.getPosition();
		}
		//Get the 3D position of top left corner
		FlatPoint pos1 = camera.translatePoint(rendPos);
		FlatPoint pos2 = camera.translatePoint(rendPos.plus(this.getSize()));
		//canvas.drawRect(pos1.x, pos1.y, pos2.x, pos2.y, paint);
		canvas.drawBitmap(image, getFrame(),
						  new RectF(pos1.x, pos1.y, pos2.x, pos2.y),
						  paint);
	}
	
	public void pauseAnimation()
	{
		currentAnimation.pause();
	}
	
	protected class Animation
	{
		Point[] points;
		private int currentFrame = 0;
		private boolean playing = false;
		int skipFrames = 0;
		int framesSkipped = 0;
		
		public Animation(Point[] points)
		{
			this.points = points;
		}
		public Animation(int yPos, int frameSkip)
		{
			this.skipFrames = frameSkip;
			int frames = image.getWidth()/frameSize.x;
			ArrayList<Point> tempPoints = new ArrayList<Point>();
			for(int i=0; i<frames*frameSize.x; i+=frameSize.x)
			{
				tempPoints.add(new Point(i,yPos));
			}
			this.points = tempPoints.toArray(new Point[tempPoints.size()]);
		}
		public Animation(int yPos)
		{
			this(yPos, 0);
		}
		
		public void update()
		{
			if(playing)
			{
				if (framesSkipped == skipFrames)
				{
					if(currentFrame >= points.length-1)
					{
						currentFrame = 0;
					}
					else
					{
						currentFrame++;
					}
					framesSkipped = 0;
				}
				else
				{
					framesSkipped++;
				}
				
			}
			
		}
		public void play()
		{
			playing = true;
		}
		public void pause()
		{
			playing = false;
		}
		public void stop()
		{
			playing = false;
			currentFrame = 0;
		}
		public void setFrameSkip(int skip)
		{
			this.skipFrames = skip;
		}
		public Point getFrame()
		{
			return points[currentFrame];
		}
	}
}
