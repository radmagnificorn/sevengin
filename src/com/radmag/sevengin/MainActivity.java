package com.radmag.sevengin;

import android.app.*;
import android.content.pm.*;
import android.os.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.testgame.*;

public class MainActivity extends Activity
{
   
	private Game game;
	private boolean running = true;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		game = new TestGame(this);
		
    }

	@Override
	public void onPause()
	{
		super.onPause();
		game.pause();
	}
	
	public void onResume()
	{
		super.onResume();
		running = true;
		game.start();
	}
	
}
