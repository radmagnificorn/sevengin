package com.radmag.sevengin;

import android.graphics.*;
import com.radmag.sevengin.utility.*;

/**
* Used to calculate 3d perspective and frame areas
*/
public class GinCamera
{
	private GinPoint viewpoint;
	private GinPoint position;
	private GinPoint offsetPoint;
	private FlatPoint resolution;
	private FlatPoint currentOffset = new FlatPoint();
	
	/**
	* @param resolution used to calculate viewport offset
	*/
	public GinCamera(FlatPoint resolution)
	{
			float xCenter = resolution.x/2.0f;
			float yCenter = resolution.y/2.0f;
			viewpoint = new GinPoint(xCenter, yCenter, -1000);
			position = new GinPoint();
			offsetPoint = new GinPoint();
			currentOffset = new FlatPoint();
			this.resolution = resolution;
	}
	public GinCamera()
	{
		viewpoint = new GinPoint();
		position = new GinPoint();
	}
	
	/**
	* translate a 3D point to 2D render coordinates
	*
	* @param orgPoint 3D point to translate
	* @param offset offset point by this amount (used internally)
	*/
	public FlatPoint translatePoint(GinPoint orgPoint, FlatPoint offset)
	{
		//Translate 3D point to 2D render coordinates
		
		FlatPoint point = new FlatPoint();
		orgPoint = orgPoint.plus(position);
		//find where ray intersects screen on y axis
		float yRaySlope = (orgPoint.y - viewpoint.y)/(orgPoint.z - viewpoint.z);
		point.y = yRaySlope * (0 - orgPoint.z) + orgPoint.y;
		//find where ray intersects screen on x axis
		float xRaySlope = (orgPoint.x - viewpoint.x)/(orgPoint.z - viewpoint.z);
		point.x = xRaySlope * (0 - orgPoint.z) + orgPoint.x;
		
		return point.plus(offset);   
	}
	/**
	 * translate a 3D point to 2D render coordinates
	 *
	 * @param orgPoint 3D point to translate
	 */
	public FlatPoint translatePoint(GinPoint orgPoint)
	{
		return translatePoint(orgPoint, this.currentOffset);
	}
	/**
	 * translate a 3D point to 2D render coordinates
	 *
	 * @param orgPoint 3D point to translate
	 * @param useoffset apply camera offset(recommended)
	 */
	public FlatPoint translatePoint(GinPoint orgPoint, boolean useoffset)
	{
		FlatPoint retPoint;
		if (useoffset)
		{
			retPoint = this.currentOffset;
		}
		else
		{
			retPoint = new FlatPoint(0,0);
		}
		return translatePoint(orgPoint, retPoint);
	}
	
	public GinPoint getPosition()
	{
		return position;
	}
	
	public FlatPoint getOffset()
	{
		return currentOffset;
		
	}
	
	/**
	* Called internally by render loop
	*/
	public void setOffset(GinPoint op)
	{
		FlatPoint off = translatePoint(op, false);
		currentOffset.x = resolution.x*0.5f - off.x;
		currentOffset.y = resolution.y*0.5f - off.y;
		
	}
	
	public void setOffset(FlatPoint op)
	{
		currentOffset = op;
	}
	
	public GinPoint getViewpoint()
	{
		return viewpoint;
	}
	
	public FlatPoint getResolution()
	{
		return resolution;
	}
}
