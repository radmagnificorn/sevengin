package com.radmag.sevengin.utility;

import android.app.*;
import android.graphics.*;
import android.util.*;
import org.xmlpull.v1.*;
import java.io.*;

public class ResourceLoader
{
	private static ResourceLoader instance;
	private Activity activity;
	
	public static ResourceLoader get()
	{
		if (instance == null)
		{
			instance = new ResourceLoader();
		}
		return instance;
	}
	
	public void init(Activity activity)
	{
		this.activity = activity;
	}
	
	public Bitmap loadBitmap(int resId)
	{
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
		return BitmapFactory.decodeResource(activity.getResources(), resId, opt);
	}
	
	public Bitmap loadBitmap(String imgPath) throws IOException
	{
		InputStream bitmap = activity.getAssets().open(imgPath);
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
		return BitmapFactory.decodeStream(bitmap);
	}
	
	public XmlPullParser loadXML(int resId)
	{
		return activity.getResources().getXml(resId);
	}
	
	public XmlPullParser loadXML(String xmlPath) throws IOException
	{
		InputStream xmlStream = activity.getAssets().open(xmlPath);
		XmlPullParser areaParser = null;
		try
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			areaParser = factory.newPullParser();
			areaParser.setInput(xmlStream, "UTF-8");
		}
		catch (XmlPullParserException e)
		{
			Log.e("SevenGin", e.getMessage());
		}

		return areaParser; 
	}
}
