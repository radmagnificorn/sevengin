package com.radmag.sevengin.utility;

import android.graphics.*;

public class FlatPoint
{
	public float x;
	public float y;

	public FlatPoint(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	public FlatPoint()
	{
		this(0,0);
	}

	public FlatPoint copy()
	{
		return new FlatPoint(x, y);
	}

	public FlatPoint plus(FlatPoint point)
	{
		return new FlatPoint(this.x + point.x,
							this.y + point.y);
	}

	public RectF plus(RectF rect)
	{
		return new RectF(this.x + rect.left,
						 this.y + rect.top,
						 this.x + rect.right,
						 this.y + rect.bottom);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("x: ").append(this.x)
			.append(" - y: ").append(this.y);
		return sb.toString();
	}


}
