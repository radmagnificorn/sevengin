package com.radmag.sevengin.utility;

public class GinPoint
{
	public float x;
	public float y;
	public float z;
	
	public GinPoint(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	public GinPoint()
	{
		this(0,0,0);
	}
	
	public GinPoint copy()
	{
		return new GinPoint(x, y, z);
	}
	
	public GinPoint plus(GinPoint point)
	{
		return new GinPoint(this.x + point.x,
						this.y + point.y,
						this.z + point.z);
	}
	
	public GinPoint plus(FlatPoint point)
	{
		return new GinPoint(this.x + point.x,
							this.y + point.y,
							this.z);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("x: ").append(this.x)
		.append(" - y: ").append(this.y)
		.append(" - z: ").append(this.z);
		return sb.toString();
	}
	
}
