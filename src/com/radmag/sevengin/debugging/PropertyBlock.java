package com.radmag.sevengin.debugging;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.utility.*;
import java.util.*;

public class PropertyBlock extends Sprite
{
	private HashMap<String, Object> properties = new HashMap<String, Object>();
	Paint paint = new Paint();
	
	public PropertyBlock(GinPoint position)
	{
		super(position);
	}
	
	public void setProperty(String name, Object value)
	{
		properties.put(name, value);
	}
	
	@Override 
	public void update()
	{
		
	}
	
	@Override
	public void render(Canvas canvas, GinCamera camera, float tc)
	{
		paint.setColor(Color.WHITE);
		float linepos = 0;
		
		for(String key: properties.keySet())
		{
			String line = key + ": " + properties.get(key).toString();
			canvas.drawText(line, getPosition().x, getPosition().y + linepos, paint);
			linepos += 25;
		}
		
	}
}
