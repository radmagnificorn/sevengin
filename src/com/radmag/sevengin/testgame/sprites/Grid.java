package com.radmag.sevengin.testgame.sprites;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.controller.*;
import com.radmag.sevengin.utility.*;

public class Grid extends Sprite
{

	Paint paint = new Paint();
	Controller controller = Controller.get();
	
	public Grid(GinPoint position, GinPoint size)
	{
		super(position,size, new GinPoint(0,0,0));
		paint.setColor(Color.BLUE);
	}
	
	@Override
	public void render(Canvas canvas, GinCamera camera, float tc)
	{
		
		//Draw vertical lines
		for(int i=0; i<this.getSize().x; i+=50)
		{
			GinPoint p1 = this.getPosition().copy();
			p1.x += i;
			GinPoint p2 = p1.copy();
			p2.z -= this.getSize().y;
			canvas.drawLine(camera.translatePoint(p1).x,
				camera.translatePoint(p1).y,
				camera.translatePoint(p2).x,
				camera.translatePoint(p2).y, paint);
		}
		
		//Draw horizontal lines
		for(int i=0; i<this.getSize().y; i+=50)
		{
			GinPoint p1 = this.getPosition().copy();
			p1.z -= i;
			GinPoint p2 = p1.copy();
			p2.x += this.getSize().x;
			canvas.drawLine(camera.translatePoint(p1).x,
							camera.translatePoint(p1).y,
							camera.translatePoint(p2).x,
							camera.translatePoint(p2).y, paint);
		}
	}
	
	@Override
	public void update()
	{
		
	}
}
