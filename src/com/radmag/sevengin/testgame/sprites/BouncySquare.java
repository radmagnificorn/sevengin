package com.radmag.sevengin.testgame.sprites;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.utility.*;

public class BouncySquare extends Sprite
{
	private GinPoint lastRenderPoint;
	
	public BouncySquare()
	{
		this(new GinPoint(50, 50, 200),
				new GinPoint(100, 100, 0),
				new GinPoint(20, 20, 10));
	}
	
	public BouncySquare(GinPoint pos, GinPoint size, GinPoint vel)
	{
		super(pos, size, vel);
		lastRenderPoint = pos.copy();
	}
	
	@Override
	public void update()
	{
		GinPoint position = this.getPosition();
		GinPoint velocity = this.getVelocity();
		if (position.x + this.getSize().x > 1280)
		{
			velocity.x = velocity.x * -1;
		}
		if (position.x < 0)
		{
			velocity.x = Math.abs(velocity.x);
		}
		
		if (position.y + this.getSize().y > 550)
		{
			velocity.y = velocity.y * -1;
		}
		if (position.y < 0)
		{
			velocity.y = Math.abs(velocity.y);
		}
		
		if (position.z + this.getSize().z > 500)
		{
			velocity.z = velocity.z * -1;
		}
		if (position.z < -500)
		{
			velocity.z = Math.abs(velocity.z);
		}
		
		position.x += velocity.x;		
		position.y += velocity.y;
		position.z += velocity.z;
	}
	
	@Override
	public void render(Canvas canvas, GinCamera camera, float tc)
	{
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		
		//Get the interpolated position
		GinPoint rendPos = this.getInterpPosition(tc);
		//Get the 3D position of top left corner
		FlatPoint pos1 = camera.translatePoint(rendPos);
		FlatPoint pos2 = camera.translatePoint(rendPos.plus(this.getSize()));
		canvas.drawOval(new RectF(pos1.x, pos1.y, pos2.x, pos2.y), paint);
	}
}
