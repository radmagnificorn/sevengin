package com.radmag.sevengin.testgame.sprites;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.utility.*;
import java.util.*;

public class Cube extends BouncySquare
{
	
	private ArrayList<DoublePoint> points = new ArrayList<DoublePoint>();
	private Paint paint = new Paint();
	
	public Cube(GinPoint position)
	{
		this.setSize(new GinPoint(200,200,200));
		this.setPosition(position);
		points.add(new DoublePoint(new GinPoint(0, 0, 0), new GinPoint(200, 0, 0)));
		points.add(new DoublePoint(new GinPoint(200, 0, 0), new GinPoint(200, 200, 0)));
		points.add(new DoublePoint(new GinPoint(200, 200, 0), new GinPoint(0, 200, 0)));
		points.add(new DoublePoint(new GinPoint(0, 200, 0), new GinPoint(0, 0, 0)));

		points.add(new DoublePoint(new GinPoint(0, 0, 200), new GinPoint(200, 0, 200)));
		points.add(new DoublePoint(new GinPoint(200, 0, 200), new GinPoint(200, 200, 200)));
		points.add(new DoublePoint(new GinPoint(200, 200, 200), new GinPoint(0, 200, 200)));
		points.add(new DoublePoint(new GinPoint(0, 200, 200), new GinPoint(0, 0, 200)));

		points.add(new DoublePoint(new GinPoint(0, 0, 0), new GinPoint(0, 0, 200)));
		points.add(new DoublePoint(new GinPoint(200, 0, 0), new GinPoint(200, 0, 200)));
		points.add(new DoublePoint(new GinPoint(200, 200, 0), new GinPoint(200, 200, 200)));
		points.add(new DoublePoint(new GinPoint(0, 200, 0), new GinPoint(0, 200, 200)));
		
		paint.setColor(Color.WHITE);
	}
	
	@Override
	public void render(Canvas canvas, GinCamera camera, float tc)
	{
		for (DoublePoint dp : points)
		{
			FlatPoint tp1 = camera.translatePoint(dp.p1.plus(this.getInterpPosition(tc)));
			FlatPoint tp2 = camera.translatePoint(dp.p2.plus(this.getInterpPosition(tc)));
			canvas.drawLine(tp1.x, tp1.y, tp2.x, tp2.y, paint);
		}
	}
	
	class DoublePoint
	{
		public GinPoint p1;
		public GinPoint p2;
		
		public DoublePoint(GinPoint p1, GinPoint p2)
		{
			this.p1 = p1;
			this.p2 = p2;
		}
	}
}
