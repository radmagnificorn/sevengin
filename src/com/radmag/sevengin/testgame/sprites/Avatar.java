package com.radmag.sevengin.testgame.sprites;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.collision.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.controller.*;
import com.radmag.sevengin.utility.*;


public class Avatar extends AnimatedSprite
{
	private BoundingMap bounds;
	private Controller controller;
	float walkSpeed = 20;
		
	public Avatar(GinPoint position, GinPoint size)
	{
		super(position, size, new GinPoint(0, 0, 0));
		this.frameSize = new Point(75, 75);
		image = ResourceLoader.get().loadBitmap(R.drawable.suitsprites);
		animations.put("walk left", new Animation(0, 1));
		animations.put("walk right", new Animation(75, 1));
		animations.put("walk toward", new Animation(150, 1));
		animations.put("walk away", new Animation(225, 1));
		animations.put("face toward", new Animation(new Point[]{new Point(0,300)}));
		animations.put("face away", new Animation(new Point[]{new Point(75,300)}));
		animations.put("face right", new Animation(new Point[]{new Point(150,300)}));
		animations.put("face left", new Animation(new Point[]{new Point(225,300)}));
		this.setAnimation("face toward");
		controller = Controller.get();
	}
	public Avatar()
	{
		this(new GinPoint(500, 350, 0), new GinPoint(200, 200, 0));
	}
	
	@Override
	public void render(Canvas canvas, GinCamera camera, float tc)
	{
		//manually set offset with no interpolation
		FlatPoint off = camera.translatePoint(this.getPosition(), false);
		off.x = camera.getResolution().x*0.5f - off.x;
		off.y = camera.getResolution().y*0.5f - off.y;
		//Get the 3D position of top left corner
		FlatPoint pos1 = camera.translatePoint(this.getPosition(), off);
		FlatPoint pos2 = camera.translatePoint(this.getPosition().plus(this.getSize()), off);
		
		
		//canvas.drawRect(pos1.x, pos1.y, pos2.x, pos2.y, paint);
		canvas.drawBitmap(image, getFrame(),
						  new RectF(pos1.x, pos1.y, pos2.x, pos2.y),
						  paint);
		//super.render(canvas, camera, 0);
	}
	
	@Override
	public void update()
	{
		
		//standing logic
		if(controller.getRight() == ButtonState.ON_UP)
		{
			getVelocity().x = 0;
			if (!(controller.getUp() == ButtonState.DOWN
			|| controller.getDown() == ButtonState.DOWN))
			{
				this.playAnimation("face right");
			}
		}
		if(controller.getLeft() == ButtonState.ON_UP)
		{
			getVelocity().x = 0;
			if (!(controller.getUp() == ButtonState.DOWN
			|| controller.getDown() == ButtonState.DOWN))
			{
				this.playAnimation("face left");
			}
		}
		if(controller.getUp() == ButtonState.ON_UP) 
		{
			getVelocity().z = 0;
			if (!(controller.getLeft() == ButtonState.DOWN
			|| controller.getRight() == ButtonState.DOWN))
			{
				this.playAnimation("face away");
			}
		}
		if(controller.getDown() == ButtonState.ON_UP) 
		{
			getVelocity().z = 0;
			if (!(controller.getLeft() == ButtonState.DOWN
			|| controller.getRight() == ButtonState.DOWN))
			{
				this.playAnimation("face toward");
			}
		}
		
		//walking logic
		if(controller.getRight() == ButtonState.ON_DOWN)
		{
			getVelocity().x = walkSpeed;
			this.playAnimation("walk right");
		}
		
		if(controller.getLeft() == ButtonState.ON_DOWN)
		{
			getVelocity().x = -walkSpeed;
			this.playAnimation("walk left");
		}
		
		if(controller.getUp() == ButtonState.ON_DOWN) 
		{
			getVelocity().z = walkSpeed;
			this.playAnimation("walk away");
		}
		
		if(controller.getDown() == ButtonState.ON_DOWN) 
		{
			getVelocity().z = -walkSpeed;
			this.playAnimation("walk toward");
		}
		
		//adjust diagonal speed
		if (getVelocity().x != 0 && getVelocity().z != 0)
		{
			if (getVelocity().x > 0)
			{
				getVelocity().x = walkSpeed * 0.6f;
			} else {
				getVelocity().x = -walkSpeed * 0.6f;
			}
			if (getVelocity().z > 0)
			{
				getVelocity().z = walkSpeed * 0.6f;
			} else {
				getVelocity().z = -walkSpeed * 0.6f;
			}
		}
		
		super.update();
		//this.setPosition(getPosition().plus(getVelocity()));
	}
	
	public void setBounds(BoundingMap bounds)
	{
		this.bounds = bounds;
	}
	
	public void setWalkSpeed(float speed)
	{
		this.walkSpeed = speed;
	}
	
}
