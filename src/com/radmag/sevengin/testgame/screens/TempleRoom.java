package com.radmag.sevengin.testgame.screens;

import android.graphics.*;
import android.util.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.debugging.*;
import com.radmag.sevengin.scene.*;
import com.radmag.sevengin.utility.*;
import java.io.*;

public class TempleRoom extends Area
{
	
	private PropertyBlock debugProps = new PropertyBlock(
					new GinPoint(20,20,0));
	
	public TempleRoom(FlatPoint screenRes)
	{
		super("templeroom/templeroom.xml", screenRes);
		camera.getViewpoint().y = 499;
		camera.getPosition().y = -104;
		camera.getViewpoint().x = 1280;
		//this.setBackground(ResourceLoader.get().loadBitmap(R.drawable.castlehallbig));
		/*
		try
		{
			this.setBackground(ResourceLoader.get().loadBitmap("templeroom/castlehallbig.png"));
		}
		catch (IOException e)
		{
			Log.e("SevenGin", "Unable to load background: " + e.getMessage());
		}
		*/
		avatar.setWalkSpeed(50);	
		Sprite well = new Sprite(new GinPoint(1230, 528, 2230),
			new GinPoint(197, 286, 0),
			new GinPoint(0,0,0));
		well.setImage(ResourceLoader.get().loadBitmap(R.drawable.wellsprite));
		this.AddComponent(well);
		this.AddComponent(debugProps);
		 
		debugProps.setProperty("Camera Pos", this.getCamera().getPosition());
		debugProps.setProperty("Camera offset", this.getCamera().getOffset());
		debugProps.setProperty("Camera Vpoint", this.getCamera().getViewpoint());
	
	}
	
	@Override
	public void render(Canvas canvas, float tc)
	{
		camera.setOffset(avatar.getInterpPosition(tc));
		super.render(canvas, tc);
		//mbc.render(canvas);
	}
	
	@Override
	public void update()
	{
		float yVel = 0;
		if (controller.isAction())
		{
			yVel = -5;
		} else if (controller.isCancel()) {
			yVel = 5;
		} else {
			yVel = 0;
		}
		debugProps.setProperty("Left Color", mbc.leftColor);
		//debugProps.setProperty("Avatar Pos", avatar.getPosition());
		camera.getViewpoint().y += yVel;
		//camera.getPosition().y += yVel;
		
		//camera.setOffset(avatar.getPosition());
		super.update();
	}
}
