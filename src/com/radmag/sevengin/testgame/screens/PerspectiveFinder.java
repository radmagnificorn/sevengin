package com.radmag.sevengin.testgame.screens;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.controller.*;
import com.radmag.sevengin.debugging.*;
import com.radmag.sevengin.testgame.sprites.*;
import com.radmag.sevengin.utility.*;

public class PerspectiveFinder extends Screen
{
	private Controller controller = Controller.get();
	private PropertyBlock debugProps = new PropertyBlock(
					new GinPoint(20,20,0));
	private Avatar avatar;
	
	private boolean isViewport = true;
	
	public PerspectiveFinder(FlatPoint screenRes)
	{
		super(screenRes);
		
		this.setBackground(ResourceLoader.get().loadBitmap(R.drawable.castlehallbig));
		avatar = new Avatar();
		
		//avatar.getPosition().x = avatar.getPosition().x * 2;
		//avatar.getPosition().y = avatar.getPosition().y * 2;
		
		this.AddComponent(avatar);
		
		this.AddComponent(new Grid(new GinPoint(0,550, 1000), 
								new GinPoint(1280, 1800, 0)));
		this.AddComponent(debugProps);
		debugProps.setProperty("Avatar Pos", avatar.getPosition()); 
		debugProps.setProperty("Camera Pos", this.getCamera().getPosition());
		debugProps.setProperty("Camera Offset", this.getCamera().getOffset());
		debugProps.setProperty("Camera Vpoint", this.getCamera().getViewpoint());
	}
	
	@Override
	public void update()
	{
		float yVel = 0;
		if (controller.isAction())
		{
			yVel = -1;
		} else if (controller.isCancel()) {
			yVel = 1;
		} else {
			yVel = 0;
		}
		if (controller.isX())
		{
			camera.getViewpoint().y += 1;
		}
		if (controller.isY()) {
			camera.getViewpoint().y -= 1;
		}
		
		camera.getPosition().y += yVel;
		super.update();
	}
}
