package com.radmag.sevengin.testgame.screens;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.controller.*;
import com.radmag.sevengin.testgame.sprites.*;
import com.radmag.sevengin.utility.*;

public class BouncyRoom extends Screen
{
	private Controller controller = Controller.get();
	public BouncyRoom(FlatPoint screenRes)
	{
		super(screenRes);
		
		this.setBackground(ResourceLoader.get().loadBitmap(R.drawable.earth));
		
		BouncySquare purpleBounce = new BouncySquare();
		this.AddComponent(purpleBounce);
		this.AddComponent(new BouncySquare(new GinPoint(100, 100, 0),
											   new GinPoint(100, 100, 0),
											   new GinPoint(30, 30, 30)));
		BouncySquare greenSquare = new BouncySquare();
		greenSquare.setVelocity(new GinPoint(7, 4, 25));
		greenSquare.setSize((new GinPoint(100, 100, 0)));
		this.AddComponent(greenSquare);
		this.AddComponent(new Avatar());
		
		//this.AddComponent(new Cube(new GinPoint(0,0,0)));
		/*
		this.AddComponent(new Grid(new GinPoint(0,550, 1000), 
								new GinPoint(1280, 1800, 0)));
		this.AddComponent(new Grid(new GinPoint(0,0,1000),
								new GinPoint(1280, 1800, 0)));
		*/
	}
	
	@Override
	public void update()
	{
		float yVel = 0;
		if (controller.isAction())
		{
			yVel = -5;
		} else if (controller.isCancel()) {
			yVel = 5;
		} else {
			yVel = 0;
		}
		camera.getPosition().y += yVel;
		super.update();
	}
}
