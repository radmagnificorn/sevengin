package com.radmag.sevengin.testgame;

import android.app.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.testgame.screens.*;
import com.radmag.sevengin.utility.*;

public class TestGame extends Game
{
	public TestGame(Activity activity)
	{
		super(activity);		
		FlatPoint res = new FlatPoint(this.getResolution().x, this.getResolution().y);
		this.setScreen(new TempleRoom(res));
	}
}
