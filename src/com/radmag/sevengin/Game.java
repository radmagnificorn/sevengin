package com.radmag.sevengin;

import android.app.*;
import android.os.*;
import android.util.*;
import android.view.*;
import com.radmag.sevengin.controller.*;
import com.radmag.sevengin.utility.*;

public class Game extends Thread
{
	
	private Screen currentScreen;
	
	public static final short ticksPerSecond = 24;
	public static final short skipTicks = 1000 / ticksPerSecond;
	private long startTime = 0;
	private boolean running = false;
	private Controller controller;
	private GinView view;
	private Display display;
	private AndroidTouchPadAdapter touchpad;
	
	private FlatPoint resolution = new FlatPoint(1280.0f, 720.0f);
	
	public Game(Activity activity)
	{
		display = activity.getWindowManager().getDefaultDisplay();
		touchpad = new AndroidTouchPadAdapter(this.getScale());
		view = new GinView(activity, display, this);
		ResourceLoader.get().init(activity);
		activity.setContentView(view);
		controller = Controller.get();
	}
	
	@Override
	public void run()
	{
		Log.d("SevenGin", "Running Application");
		running = true;
		view.onResume();
		while(running)
		{
			startTime = SystemClock.uptimeMillis();
			controller.update();
			currentScreen.update();
			long timeDiff = SystemClock.uptimeMillis() - startTime;
			if (timeDiff < skipTicks)
			{
				try
				{
					this.sleep(skipTicks - timeDiff);
				}
				catch (InterruptedException e)
				{}
			}
		}
	}
	
	public void pause()
	{
		view.onPause();
		running = false;
	}
	
	public float getTickCompletion()
	{
		return (float)getCurrentTick() / skipTicks;
	}
	public short getCurrentTick()
	{
		return (short)(SystemClock.uptimeMillis() - startTime);
	}
	
	public void setScreen(Screen screen)
	{
		currentScreen = screen;
	}
	
	public Screen getScreen()
	{
		return this.currentScreen;
	}
	
	public AndroidTouchPadAdapter getTouchPad()
	{
		return touchpad;
	}
	
	public void setResolution(FlatPoint res)
	{
		resolution = res;
	}
	
	public FlatPoint getResolution()
	{
		return resolution;
	}
	
	public float getScale()
	{
		return resolution.y/display.getHeight();
	}
	
}
