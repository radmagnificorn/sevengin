package com.radmag.sevengin.controller;

import android.view.*;

public class AndroidKeyboardAdapter implements View.OnKeyListener
{
	Controller controller;
	
	public AndroidKeyboardAdapter(Controller controller)
	{
		this.controller = controller;
	}
	
	public boolean onKey(View view, int keyCode, KeyEvent e)
	{
		ButtonState state = ButtonState.UP;
		if (e.getAction() == KeyEvent.ACTION_DOWN)
		{
			state = ButtonState.ON_DOWN;
		}
		else if (e.getAction() == KeyEvent.ACTION_UP)
		{
			state = ButtonState.ON_UP;
		}
		
		switch (keyCode)
		{
			case KeyEvent.KEYCODE_W:
				controller.setUp(state);
				break;
			case KeyEvent.KEYCODE_A:
				controller.setLeft(state);
				break;
			case KeyEvent.KEYCODE_S:
				controller.setDown(state);
				break;
			case KeyEvent.KEYCODE_D:
				controller.setRight(state);
				break;
			default:
				//Do nothing
		}
		return false;
	}
	
}
