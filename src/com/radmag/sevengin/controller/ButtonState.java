package com.radmag.sevengin.controller;

public enum ButtonState
{
	UP,
	DOWN,
	ON_UP,
	ON_DOWN
}
