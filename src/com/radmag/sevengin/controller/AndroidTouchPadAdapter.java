package com.radmag.sevengin.controller;

import android.graphics.*;
import android.view.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.utility.*;

public class AndroidTouchPadAdapter extends Sprite implements View.OnTouchListener
{
	private Controller controller;
	//private ArrayList<TouchButton> buttons = new ArrayList<TouchButton>();
	private TouchButton[] buttons;
	private FlatPoint position = new FlatPoint(50, 400);
	private Paint paint = new Paint();
	private float scale = 1;

	public AndroidTouchPadAdapter(float scale)
	{
		controller = Controller.get();
		this.scale = scale;
		buttons = new TouchButton[] {
			new TouchButton(0, 0, 300, 100, controller.getUpButton()),
			new TouchButton(0, 200, 300, 300, controller.getDownButton()),
			new TouchButton(0, 0, 100, 300, controller.getLeftButton()),
			new TouchButton(200, 0, 300, 300, controller.getRightButton()),
			new TouchButton(900, 200, 970, 270, ButtonStyle.CIRCLE, controller.getActionButton()),
			new TouchButton(1050, 150, 1120, 220, ButtonStyle.CIRCLE, controller.getCancelButton()),
			new TouchButton(700, 150, 770, 220, ButtonStyle.CIRCLE, controller.getXButton()),
			new TouchButton(850, 100, 920, 170, ButtonStyle.CIRCLE, controller.getYButton())
		};


	}


	public boolean onTouch(View p1, MotionEvent me)
	{
		GinView view = (GinView)p1;
		//debugString = me.getX() + " : " + me.getY();
		int touches = me.getPointerCount();
		for (int itouch=0; itouch < touches; itouch++)
		{
			for (int i=0; i < buttons.length; i++)
			{
				TouchButton ibutton = buttons[i];
				boolean contained = ibutton.contains(me.getX(itouch) * scale, me.getY(itouch) * scale);
				ButtonState state = null;
				switch (me.getActionMasked())
				{
					case MotionEvent.ACTION_DOWN:
					case MotionEvent.ACTION_POINTER_DOWN:
						if (contained)
						{state = ButtonState.ON_DOWN;}
						break;
					case MotionEvent.ACTION_MOVE:
						if (ibutton.getState() == ButtonState.DOWN && !contained)
						{
							state = ButtonState.ON_UP;
						}
						if (ibutton.getState() == ButtonState.UP && contained)
						{
							state = ButtonState.ON_DOWN;
						}
						break;
					case MotionEvent.ACTION_UP:
					case MotionEvent.ACTION_POINTER_UP:
						if (contained)
						{state = ButtonState.ON_UP;}
						break;
					default:
						//Do Nothing

				}
				if (state != null)
				{
					ibutton.setState(state);
				}
			}	
		}
		//view.setInterpolation(!view.getInterpolation());
		//controller.setAction(ButtonState.DOWN);
		return true;
	}

	@Override
	public void render(Canvas canvas, GinCamera camera, float interp)
	{
		paint.setColor(Color.WHITE);
		paint.setAlpha(50);
		for (int i=0; i < buttons.length; i++)
		{
			TouchButton button = buttons[i];
			if (button.getStyle() == ButtonStyle.SQUARE)
			{
				canvas.drawRect(button, paint);
			}
			if (button.getStyle() == ButtonStyle.CIRCLE)
			{
				canvas.drawOval(button, paint);
			}



		}
	}

	private class TouchButton extends RectF
	{
		private ButtonStyle style = ButtonStyle.SQUARE;
		private ControllerButton button;

		public TouchButton(float x1, float y1, float x2, float y2, ControllerButton button)
		{
			this(x1, y1, x2, y2, ButtonStyle.SQUARE, button);
		}
		public TouchButton(float x1, float y1, float x2, float y2, ButtonStyle style, ControllerButton button)
		{
			super(position.plus(new RectF(x1, y1, x2, y2)));
			this.style = style;
			this.button = button;
		}

		public ButtonStyle getStyle()
		{
			return style;
		}

		public void setState(ButtonState state)
		{
			button.setState(state);
		}
		public ButtonState getState()
		{
			return button.getState();
		}

	}

	enum ButtonStyle
	{
		SQUARE,
		CIRCLE,
		INVISIBLE
		}


}
