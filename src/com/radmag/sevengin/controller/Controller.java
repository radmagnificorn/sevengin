package com.radmag.sevengin.controller;

import java.util.*;

public class Controller {
	
	private List<ControllerButton> buttons = new ArrayList<ControllerButton>();
	
	private ControllerButton upButton = new ControllerButton();
	private ControllerButton downButton = new ControllerButton();
	private ControllerButton leftButton = new ControllerButton();
	private ControllerButton rightButton = new ControllerButton();
	private ControllerButton actionButton = new ControllerButton();
	private ControllerButton cancelButton = new ControllerButton();
	private ControllerButton xButton = new ControllerButton();
	private ControllerButton yButton = new ControllerButton();
	
	public Controller()
	{
		buttons.add(upButton);
		buttons.add(downButton);
		buttons.add(leftButton);
		buttons.add(rightButton);
		buttons.add(actionButton);
		buttons.add(cancelButton);
		buttons.add(xButton);
		buttons.add(yButton);
	}
	
	private static Controller instance;

	public ControllerButton getUpButton()
	{
		return upButton;
	}
	public ControllerButton getDownButton()
	{
		return downButton;
	}
	public ControllerButton getLeftButton()
	{
		return leftButton;
	}
	public ControllerButton getRightButton()
	{
		return rightButton;
	}
	public ControllerButton getActionButton()
	{
		return actionButton;
	}
	public ControllerButton getCancelButton()
	{
		return cancelButton;
	}
	public ControllerButton getXButton()
	{
		return xButton;
	}
	public ControllerButton getYButton()
	{
		return yButton;
	}
	
	public static Controller get()
	{
		if (instance == null)
		{
			instance = new Controller();
		}
		
		return instance;
	}

	public boolean isUp() {
		return (upButton.getState() == ButtonState.DOWN || upButton.getState() == ButtonState.ON_DOWN);
	}
	
	public ButtonState getUp()
	{
		return upButton.getState();
	}

	public void setUp(ButtonState stateIn) {
		upButton.setState(stateIn);
	}

	public boolean isDown() {
		ButtonState bState = downButton.getState();
		return (bState == ButtonState.DOWN || bState == ButtonState.ON_DOWN);
	}
	
	public void update()
	{
		// update buffered presses on all buttons
		for(ControllerButton cb: buttons)
		{
			cb.update();
		}
	}
	
	

	public void setDown(ButtonState stateIn) {
		downButton.setState(stateIn);
	}
	
	public ButtonState getDown()
	{
		return downButton.getState();
	}

	public boolean isLeft() {
		ButtonState bState = leftButton.getState();
		return (bState == ButtonState.DOWN || bState == ButtonState.ON_DOWN);
	}
	

	public void setLeft(ButtonState stateIn) {
		leftButton.setState(stateIn);
	}
	
	public ButtonState getLeft()
	{
		return leftButton.getState();
	}

	public boolean isRight() {
		ButtonState bState = rightButton.getState();
		return (bState == ButtonState.DOWN || bState == ButtonState.ON_DOWN);
	}
	
	

	public void setRight(ButtonState stateIn) {
		rightButton.setState(stateIn);
	}
	
	public ButtonState getRight()
	{
		return rightButton.getState();
	}

	public boolean isAction() {
		ButtonState bState = actionButton.getState();
		return (bState == ButtonState.DOWN || bState == ButtonState.ON_DOWN);
	}
	
	public ButtonState getAction()
	{
		return actionButton.getState();
	}
	

	public void setAction(ButtonState stateIn) {
		actionButton.setState(stateIn);
	}

	public boolean isCancel() {
		ButtonState bState = cancelButton.getState();
		return (bState == ButtonState.DOWN || bState == ButtonState.ON_DOWN);
	}
	
	public ButtonState getCancel()
	{
		return cancelButton.getState();
	}
	

	public void setCancel(ButtonState stateIn) {
		cancelButton.setState(stateIn);
	}
	
	public boolean isX()
	{
		ButtonState bState = xButton.getState();
		return (bState == ButtonState.DOWN || bState == ButtonState.ON_DOWN);
	}
	public boolean isY()
	{
		ButtonState bState = yButton.getState();
		return (bState == ButtonState.DOWN || bState == ButtonState.ON_DOWN);
	}
		
}


