package com.radmag.sevengin.controller;

public class ControllerButton {
	
	// buffer button presses to deal with asynchronous
	// button press events.
	private ButtonState bufferState = ButtonState.UP;
	private ButtonState activeState = ButtonState.UP;
	
	public ControllerButton()
	{
		
	}
	
	public ButtonState getState()
	{
		return activeState;
	}
	
	public void setState(ButtonState stateIn)
	{
		bufferState = stateIn;
	}
	
	public void update()
	{
		activeState = bufferState;
		
		// If the last cycle it was a change event
		// change it to a stable state
		if (activeState == ButtonState.ON_UP)
		{
			bufferState = ButtonState.UP;
		}
		if (activeState == ButtonState.ON_DOWN)
		{
			bufferState = ButtonState.DOWN;
		}
	}

}
