package com.radmag.sevengin.scene;

import android.graphics.*;
import com.radmag.sevengin.*;
import com.radmag.sevengin.collision.*;
import com.radmag.sevengin.components.*;
import com.radmag.sevengin.controller.*;
import com.radmag.sevengin.testgame.sprites.*;
import com.radmag.sevengin.utility.*;
import org.xmlpull.v1.*;
import android.content.res.*;
import java.io.*;
import android.os.*;
import android.util.*;

/**
 * An area map screen
 */
public class Area extends Screen
{
	protected BoundingMap boundingMap;
	protected Controller controller = Controller.get();
	protected Avatar avatar;

	protected MapBoundComponent mbc;

	/**
	 *	@param xmlId R based id for xml area definition file
	 *	@param screenRes required to create scene camera
	 */
	public Area(String areaXml, FlatPoint screenRes)
	{
		super(screenRes);
		Log.d("SevenGin", "loading area xml");
		XmlPullParser parser = null;
		try
		{
			parser = ResourceLoader.get().loadXML(areaXml);
		}
		catch (IOException e)
		{
			Log.e("SevenGin", "Unable to load area xml: " + e.getMessage());
		}
		loadAreaFromXml(parser);
		
		avatar = new Avatar(new GinPoint(1280, 800, 500), new GinPoint(800, 800, 0));
		avatar.setWalkSpeed(50);

		boundingMap = new BoundingMap(R.drawable.castlehallbndbig);
		mbc = new MapBoundComponent(boundingMap, 200, 200, camera);
		avatar.addComponent(mbc);
		this.AddComponent(avatar);
		//super(new FlatPoint(720, 1280));
	}
	public Area(FlatPoint screenRes)
	{
		super(screenRes);
		camera.getViewpoint().y = 499;
		camera.getPosition().y = -104;
		camera.getViewpoint().x = 1280;
		avatar = new Avatar(new GinPoint(1280, 800, 500), new GinPoint(800, 800, 0));
		avatar.setWalkSpeed(50);

		boundingMap = new BoundingMap(R.drawable.castlehallbndbig);
		mbc = new MapBoundComponent(boundingMap, 200, 200, camera);
		avatar.addComponent(mbc);
		this.AddComponent(avatar);

	}

	@Override
	public void render(Canvas canvas, float tc)
	{
		camera.setOffset(avatar.getInterpPosition(tc));
		super.render(canvas, tc);
		//mbc.render(canvas);
	}

	private void loadAreaFromXml(XmlPullParser parser)
	{
		Log.d("SevenGin", "entering loadAreaFromXml");
		try
		{
			int eventType = parser.getEventType();
			String parentNode = "";
			while (eventType != XmlPullParser.END_DOCUMENT)
			{
				//Log.d("SevenGin", "Event Type is " + eventType);
				switch(eventType)
				{
					case XmlPullParser.START_DOCUMENT:
						Log.d("SevenGin", "Starting Document");
						break;
					case XmlPullParser.START_TAG:
						String tagName = parser.getName();
						Log.d("SevenGin", "reading tag " + tagName);
						
						// ****CAMERA****
						if (tagName.equalsIgnoreCase("camera")||parentNode.equals("camera"))
						{
							parentNode = "camera";
							if (tagName.equalsIgnoreCase("position"))
							{
								GinPoint pos = camera.getPosition();
								pos.x = Float.parseFloat(parser.getAttributeValue(null, "x"));
								pos.y = Float.parseFloat(parser.getAttributeValue(null, "y"));
								pos.z = Float.parseFloat(parser.getAttributeValue(null, "z"));
								Log.d("SevenGin", "Set values for camera position");
							}
							
							if(tagName.equalsIgnoreCase("viewpoint"))
							{
								GinPoint vp = camera.getViewpoint();
								vp.x = Float.parseFloat(parser.getAttributeValue(null, "x"));
								vp.y = Float.parseFloat(parser.getAttributeValue(null, "y"));
								vp.z = Float.parseFloat(parser.getAttributeValue(null, "z"));
								Log.d("SevenGin", "Set values for camera viewpoint");
							}
						}
						
						// ****AVATAR****
						if (tagName.equalsIgnoreCase("avatar")||parentNode.equals("avatar"))
						{
							parentNode = "avatar";
							if (avatar == null)
							{
								avatar = new Avatar();
							}
							Log.d("SevenGin", "Parent node set to " + parentNode);
							if (tagName.equalsIgnoreCase("position"))
							{
								GinPoint apos = avatar.getPosition();
								apos.x = Float.parseFloat(parser.getAttributeValue(null, "x"));
								apos.y = Float.parseFloat(parser.getAttributeValue(null, "y"));
								apos.z = Float.parseFloat(parser.getAttributeValue(null, "z"));
								Log.d("SevenGin", "Set values for avatar viewpoint");
							}
							if (tagName.equalsIgnoreCase("size"))
							{
								Log.d("SevenGin", "Reading Size");
								GinPoint asize = avatar.getSize();
								asize.x = Float.parseFloat(parser.getAttributeValue(null, "width"));
								asize.y = Float.parseFloat(parser.getAttributeValue(null, "height"));
								asize.z = 0.0f;
							}
						}
						
						if (tagName.equalsIgnoreCase("background"))
						{
							String bgurl = parser.getAttributeValue(null, "src");
							setBackground(ResourceLoader.get().loadBitmap(bgurl));
						}
						break;
					default:
						//do nothing
				}
				eventType = parser.next();	
			}
			Log.d("SevenGin", "Successfully parsed xml");

		}
		catch (XmlPullParserException e)
		{
			Log.e("SevenGin", "Error parsing XML: " + e.getMessage());
		}
		catch (IOException e)
		{
			Log.e("SevenGin", "Error reading XML: " + e.getMessage());
		}

	}

}
