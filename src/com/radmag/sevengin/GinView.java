package com.radmag.sevengin;
import android.content.*;
import android.graphics.*;
import android.os.*;
import android.view.*;
import com.radmag.sevengin.controller.*;
import com.radmag.sevengin.utility.*;

/** SevenGin view and rendering loop runs independently of game loop.
* Frames are rendered as quickly as possible, and interpolation is used
* To smooth animation on faster devices.
*/
public class GinView extends SurfaceView implements Runnable
{
	private Paint paint;
	private SurfaceHolder surfaceHolder;
	public volatile boolean running = false;
	private Thread thread = null;
	private Context context;
	private Display display;
	private Game game;
	private float scale = 1;
	private long startTime = 0;
	
	private static final int FRAMETIME = 1000/60;
	
	private AndroidTouchPadAdapter touchpad;
	
	private boolean interpolate = true;
	
	
	public GinView(Context context, Display display, final Game game)
	{
		super(context);
		this.context = context;
		this.game = game;
		this.display = display;
		surfaceHolder = this.getHolder();
		paint = new Paint();
		paint.setColor(Color.GREEN);
		this.requestFocus();
		this.setFocusable(true);
		this.scale = display.getHeight()/game.getResolution().y;
		this.setOnTouchListener(game.getTouchPad());
		
		//this.setOnKeyListener(new AndroidKeyboardAdapter(Controller.get()));
	}
	

	public void run()
	{
		thread.setPriority(Thread.MAX_PRIORITY);
		running = true;	
		long FPS = 0;
		Bitmap testImage = ResourceLoader.get().loadBitmap(R.drawable.castlehall2);
		while (running)
		{
			startTime = SystemClock.uptimeMillis();
			if(surfaceHolder.getSurface().isValid())
			{
				Screen scene = game.getScreen();			
				Canvas canvas = surfaceHolder.lockCanvas();
				canvas.scale(scale, scale);
				canvas.drawBitmap(testImage, 0, 0, paint);
				
				float tc = game.getTickCompletion();
	
				scene.render(canvas, tc);
								
				//Render game pad
				game.getTouchPad().render(canvas, scene.getCamera(), tc);
								
				paint.setTextSize(20);
				paint.setColor(Color.WHITE);
				canvas.drawText("FPS: " + FPS, 50, 300, paint);
				//canvas.drawText("Accelerated: " + canvas.isHardwareAccelerated(), 50, 350, paint);
				
				surfaceHolder.unlockCanvasAndPost(canvas);
			
			}
			long timeDiff = (SystemClock.uptimeMillis() - startTime);
			if (timeDiff < FRAMETIME)
			{
				
				try
				{
					this.thread.sleep(FRAMETIME - timeDiff);
				}
				catch (InterruptedException e)
				{
					startTime = 0;
				}
				
			}
			long td2 = SystemClock.uptimeMillis()-startTime;
			if (td2 > 0)
			{
				FPS = 1000/td2;
			}
		}
	}
	
	public void onPause()
	{
		running = false;
	}
	
	public void onResume()
	{
		
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public boolean getInterpolation()
	{
		return interpolate;
	}
	public void setInterpolation(boolean interp)
	{
		interpolate = interp;
	}
	
	
	public static float interp(float position, float velocity, float tc)
	{
		return position + (velocity * tc);
	}
	
	private class ScreenTouch implements OnTouchListener
	{
		
		public boolean onTouch(View p1, MotionEvent p2)
		{
			GinView view = (GinView)p1;
			view.setInterpolation(!view.getInterpolation());
			
			return false;
		}
		
		
	}
	
	public AndroidTouchPadAdapter getTouchControl()
	{
		return touchpad;
	}

}
